[[basics-hands-on]]
= Kubernetes basics intro

In the following chapters, we will go through the basic concepts and objects of Kubernetes and see how to manage them.

NOTE: The Kubernetes basics session is written from a developer perspective. It focuses on what application developers need to know in order to operate Kubernetes.

As a starter, this five minutes video describes some concepts you will try out as we go along.

video::daVUONZqn88[youtube,width=600, height=338]

Link to _How Kubernetes works_ video: https://youtu.be/daVUONZqn88

We will in turn look at xref:session-basics/nodes.adoc[], xref:session-basics/pods.adoc[], xref:session-basics/replicasets.adoc[], xref:session-basics/deployment.adoc[], xref:session-basics/service.adoc[] and xref:session-basics/ingress.adoc[].

The best way to get a grip of these concepts is to create them in a Kubernetes cluster. For this we need the _minikube_ you installed earlier. The first task is hence to start minikube.

[[minikube]]
== Setting up Minikube

NOTE: For installation, please see instructions at https://minikube.sigs.k8s.io/docs/start (part 1, installation)

Start minikube by running:

.Linux
----
minikube start --memory 8g --driver=docker
----

.Mac
Follow the instructions at https://minikube.sigs.k8s.io/docs/drivers/hyperkit/

.Windows
Follow the instructions at https://minikube.sigs.k8s.io/docs/drivers/hyperv/

For other drivers, please see https://minikube.sigs.k8s.io/docs/drivers.

[TIP]
====
Optional step: Set the default driver, so you can start without `--driver=xxxx` next time:

.Linux
----
minikube config set driver docker
----

.Mac
----
minikube config set driver hyperkit
----

.Windows
----
minikube config set driver hyperv
----
====

[NOTE]
====
The step
----
Verifying Kubernetes components...
----
may take a while, just have patience.
====

Try running your first kubectl command now:

----
# Display addresses related to the cluster
kubectl cluster-info
----

Expected output (the ip-address may differ):
----
Kubernetes master is running at https://172.17.0.2:8443
KubeDNS is running at https://172.17.0.2:8443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy
----

[TIP]
====
You want minikube up and running for now, but it is still good to know how to stop it:
----
minikube stop
----
====

For more on minikube commands, please see https://minikube.sigs.k8s.io/docs/commands.
