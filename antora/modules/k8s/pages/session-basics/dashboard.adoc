= Dashboard

The dashboard is a graphical UI that allow us to perform many of the tasks we have used the command line for.

image:dashboard-1.png[]

The dashboard application is installed along with the Kubernetes cluster. Since we are using minikube, we need to run this command in order to get the dashboard up:

----
minikube dashboard
----

This should open up the dashboard in your default web browser. Browse around in it, take a look at the Deployments and Pods (see menu to the left in dashboard).

[TIP]
====
[cols="4,1"]
|===

|An action menu is available to the righ of most objects, such as a Deployment or Pod. This menu contains actions available for the actual object.

The menu for a pod, for example, has menu items for _Logs_, _Exec_, _Edit_ and _Delete_. Try them out!

|image:dashboard-2.png[]
|===
====

[NOTE]
In the next hands-on part, xref:acorn-k8s:k8s:session-distributed-services/distributed-services-in-k8s.adoc[], we will rely more on using the Dashboard.
