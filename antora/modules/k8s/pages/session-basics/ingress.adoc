= Ingress

Ingress exposes HTTP and HTTPS routes from outside the cluster to services within. Traffic routing is controlled by rules defined on the Ingress resource.

Ingress helps users access the application using a single externally accessible URL. It can be configured to route to different services within the cluster based on the URL path.

In our hello-minikube set up, we only have one service to route to. We can set up an Ingress to route to that service, which in effect allow us to use a hostname url of our choosing, like for instance _hello-minikube.example.com_.

image::ingress.png[]

. Ingress is not enabled by default in minikube. Enable it by running:
+
----
minikube addons enable ingress
----

. Wait until the ingress-pod is up and running
+
----
kubectl get pods -n kube-system
----
+
Expected output is something close to:
+
-----
NAME                                        READY   STATUS      RESTARTS   AGE
ingress-nginx-controller-7bb4c67d67-nvxp8   1/1     Running     0          2m14s
...
-----

. Create a file named `hello-minikube-ingress.yml` and add this definition:
+
[source,yaml]
----
apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
  name: hello-minikube-ingress
spec:
  rules:
    - host: hello-minikube.example.com
      http:
        paths:
          - backend:
              serviceName: hello-minikube-service
              servicePort: 80
----
+
Note that we are specifying a _rule_ with a _hostname_ pointing to our _hello-minikube-service_.

. [[etc-hosts]]Right now we do not have a DNS-server to register the domain name in, instead we will add an entry to `/etc/hosts` (`c:\Windows\System32\Drivers\etc\hosts` on Windows) where _hello-minikube.example.com_ points to the ip-address of the Kubernetes cluster.
.. First take note of the ip-address
+
----
kubectl cluster-info

# or

minikube ip
----
.. Secondly, add an entry to the hosts file (you need sudo/admin rights to edit)
+
----
# Example of entry in /etc/hosts
172.17.0.2 hello-minikube.example.com
----
.. Save the hosts file

. Try to access http://hello-minikube.example.com in browser
+
This should lead to a 404 response

. Create the Ingress
+
----
kubectl apply -f hello-minikube-ingress.yml
----

. Examine the created Ingress
+
----
kubectl get ingress

kubectl describe ingress
----

. Access http://hello-minikube.example.com again
+
It should now route you to the hello-minikube-service

== End of the first session
This marks the end of the basics walktrough. You should now have enough knowledge about Kubernetes to be dangerous, at least from a developer perspective.

In the next session, we will start looking at how to deploy a multi-container application.

== Ingress documentation

https://kubernetes.io/docs/concepts/services-networking/ingress/
