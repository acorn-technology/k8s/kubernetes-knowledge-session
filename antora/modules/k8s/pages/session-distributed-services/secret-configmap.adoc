= Secret and ConfigMap

In both _items-dep.yml_ and _reviews-dep.yml_ we configured which database the container should connect to by using the `env` properties
_spring.data.mongodb.host_, _spring.data.mongodb.port_, _spring.data.mongodb.username_ and _spring.data.mongodb.password_.

Kubernetes allows you to decouple configuration from manifest yaml files, which effectively means that it is possible to store configuration centrally, and then reference it from several locations.

In our case we can put the above database connection properties (their names and values) in a Kubernetes object called ConfigMap.

In fact, Kubernetes provides two variants to help keep your application code separate from your configuration:

* ConfigMap
* Secret

Where Secret is a special case of ConfigMap. Each of these will be presented in the coming sections.

== ConfigMap

A ConfigMap is a dictionary of configuration settings. This dictionary consists of key-value pairs of strings. Like with other dictionaries, the key lets you get and set the configuration value.

ConfigMaps are useful for storing and sharing non-sensitive, unencrypted configuration information. To use sensitive information in your clusters, you should use Secrets

There are several ways to create and populate a ConfigMap, and there are equally several ways to consume the content. The https://kubernetes.io/docs/tasks/configure-pod-container/configure-pod-configmap[official ConfigMap documentation] shows this in detail (but be prepared for information overload).

In this session we will use the arguably most straightforward way.

This is what we want to do.

* Create a ConfigMap containing all database setting except for the sensitive user and password settings.

* Change the Pod container definitions for _items_ and _reviews_ to use the values from ConfigMap.

And so we begin. A ConfigMap basically has a name, followed by a data section with key/value pairs. Let's create a yaml file representing this.

. Create a file `oblique-critique-cfgmap.yml`, put the database host and port in it.
+
[source,yaml]
----
include::acorn-k8s:k8s:example$manifests/oblique-critique-cfgmap.yml[]
----

. Apply the ConfigMap.
+
[source,bash]
----
kubectl apply -f oblique-critique-cfgmap.yml
----

. As usual with Kubernetes objects you could use standard commands on a ConfigMap:
+
[source,bash]
----
kubectl get configmap

kubectl describe configmap oblique-critique
----
+
The created object can also be found under link "Config Maps" in the Dashboard application.

That's it. The ConfigMap is in place. Now to the next task, make the Pods use the configuration. This is done by using the `valueFrom:` instead of `value:` in the `env section`, like so:

[source,yaml]
----
containers:
  - name: ...
    image: ...
    env:
      - name: spring.data.mongodb.host
        valueFrom:
          configMapKeyRef: <.>
            name: oblique-critique <.>
            key: mongodb_host <.>
----

<.> Specifies that we will take the value from a ConfigMap
<.> The name of the ConfigMap - must correspond with `.metadata.name` from the ConfigMap definition
<.> The key in the ConfigMap

. Change the items-dep.yml to look like this
+
[source,yaml]
----
include::acorn-k8s:k8s:example$manifests/items-dep-3.yml[]
----

. Then do the same with `reviews-dep.yml`

. Apply the changed files and verify that the new Pods starts with successful database connections.

== Secret

Secret is a Kubernetes object intended for storing a small amount of sensitive data. It is very similar to a ConfigMap, with the difference that the values are encoded.

NOTE: Secrets are stored as base64-encoded strings within Kubernetes, which means that they are not very secure. It is fully possible to decode the values back to readable state again. If true security is needed, one should consider something like https://www.vaultproject.io[Hashicorp Vault]

In the application we are setting up here, we currently have the database credentials in plain sight. These can be moved into a Secret, which has the benefit of centralizing the credentials to one place, and also encodes them to a non-readable state.

Kubernetes does not encode anything for us, we must handle this ourselves.

On a Linux system this is quite easy, all we need to do is to pipe the strings through `base64` like this:

.MongoDb username
----
echo -n "admin" | base64
----

This will result in: `YWRtaW4=`.

.MongoDb password
----
echo -n "password" | base64
----

This will result in: `cGFzc3dvcmQ=`.

[NOTE]
====
The equivalent in Windows is not fully as easy without installing things. Apparently, this should work in powershell:

----
powershell "[convert]::ToBase64String([Text.Encoding]::UTF8.GetBytes(\"kubernetes\"))"
----

If it doesn't work for you, just go with the values presented above.
====

Having encoded values ready put us in a position to create a secret.

. Create a file `oblique-critique-secret.yml` with the below content.
+
[source,yaml]
----
include::acorn-k8s:k8s:example$manifests/oblique-critique-secret.yml[]
----

. Apply the secret to Kubernetes
+
[source,bash]
----
kubectl apply -f oblique-critique-secret.yml
----

. We use the Secret similarly to how we did it with ConfigMap, but using `secretKeyRef` instead of `configMapKeyRef`. Edit both `items-dep.yml` and `reviews-dep.yml` as the example below.
+
[source,yaml]
----
include::acorn-k8s:k8s:example$manifests/items-dep-4.yml[]
----

. Apply the changes

The `items` and `reviews` applications are now decoupled from the config!

[NOTE]
.Sidenote
====
Secrets and ConfigMaps can be mounted as environment variables (as we just did), or as files within a container.

When ConfigMap/Secret values are mounted as files in a container, each property ends up as a file in a file structure, with its value inside.

One benefit of using this approach is that you can change a value (i.e. the content of a file) in one place, and it will get picked up by the Pods mounting it. Thus the property change will automatically propagate and take effect in all containers using the ConfigMap/Secret.

Read more at https://kubernetes.io/docs/concepts/configuration/configmap/#mounted-configmaps-are-updated-automatically[Mounted ConfigMaps are updated automatically], if you are interested.
====
