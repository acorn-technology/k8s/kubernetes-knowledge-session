= The Webapi application

The _webapi_ is the application that will be exposed to clients outside the cluster. We will later in the session set up an Ingress service that will route client calls to the webapi.

This means that the _items_ and _reviews_ applications will not be available to outside clients. The single entrance to the system is through the _webapi_.

== Backends For Frontends
The _webapi/items/reviews_ solution is influenced by an architectural pattern known as _Backends for Frontends_ (BFF). We will not dive into this pattern here, but the gist of it is as follows:

Instead of providing a single general purpose API which is used by all types of clients, like a _web application_ (written in React, Angular, Vue or whatever) or a _mobile client_ (iOS, Android), BFF is all about providing a tailored API for the web application, and another tailored API for the mobile client.

image::bff.png[0,400]

This allows each BFF to aggregate and structure the response data to the particular needs of its client. The BFF knows which services to call to do the job, effectively shielding clients from the inner complexity of the distributed service setup.

[TIP]
====
For more about BFF:
https://philcalcado.com/2015/09/18/the_back_end_for_front_end_pattern_bff.html
https://samnewman.io/patterns/architectural/bff
====

The _webapi_ application will call _items_ and _reviews_ and aggregate the data into combined responses.

The REST API looks like this:

[cols="1,1"]
|===
|Endpoint |Description

m|GET /webapi/items
|Gets all items and their reviews

m|POST /webapi/items
|Creates a new item

m|GET /webapi/items/\{id}
|Gets a specified item and its reviews

m|POST /webapi/items/\{id}/reviews
|Creates a new review for the specified item
|===

== Deployment
It is time to create yet another deployment manifest file, the last one for this session. When the webapi is deployed, we have all core application parts ready to go in the Kubernetes cluster.

The Docker image name of the _reviews_ applications is named `acorntechnology/webapi:1.0.0`.

. Create a file `webapi-dep.yml` with the below content
+
[source,yaml]
----
include::acorn-k8s:k8s:example$manifests/webapi-dep-1.yml[]
----

. Create the Deployment
+
[source,bash]
----
kubectl apply -f webapi-dep.yml
----

. Check the logs of the deployed container, either in Kubernetes Dashboard or by using `kubectl logs -f <pod name>`
+
.Example
[source,bash]
----
kubectl logs -f webapi-6bfb785754-qjcxs
----
+
The logs should actually show that the application did not start!
+
You should find an error log saying:
+
----
Caused by: java.lang.IllegalArgumentException: [The property 'services.items.url' must be configured, The property 'services.reviews.url' must be configured]
----
+
This error comes as no surprise, we have not configured the _webapi_ properly. It does not know where to find the _items_ and _reviews_ applications.

Please read on.

== Communicating with items and reviews

The _webapi_ application container exposes two properties that we must set.

* `services.items.url`
* `services.reviews.url`

As the property names imply, they define where _webapi_ should find the downstream services.

How do you reach another Pod from a Pod then? Well, as discussed in the basics hands-on xref:acorn-k8s:k8s:session-basics/service.adoc[] section, and as described in the below diagram, Pods communicate with other Pods through Services.

.The webapi app calls the services of items and reviews
image::acorn-k8s:k8s:obliquecritique-k8s-apps.png[]

All we need to do is to use the name and port of each service. The name is specified in the `.metadata.name` field in `items-svc.yml` and `reviews-svc.yml respectively.` The port is found under `.spec.ports.port` in the same files.

.Alternatively, you can run `kubectl get svc` to list the names and ports of the applied services.
[source,txt]
----
NAME           TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)   AGE
database-svc   ClusterIP   None             <none>        <none>    3d17h
items-svc      ClusterIP   10.100.167.94    <none>        80/TCP    3d15h
kubernetes     ClusterIP   10.96.0.1        <none>        443/TCP   33d
reviews-svc    ClusterIP   10.100.142.110   <none>        80/TCP    2d17h
----

Armed with this information, we can now add the needed config.

. Add the `env` section to the `webapi-dep.yml` file.
+
[source,yaml]
----
include::acorn-k8s:k8s:example$manifests/webapi-dep-2.yml[]
----

. Apply the changed Deployment
+
[source,bash]
----
kubectl apply -f webapi-dep.yml
----

. Check the logs again. The _webapi_ application should now start successfully.
+
----
Started WebapiApplication in 14.878 seconds
----

== Service

Just as for the other applications, we need to define a Service for the _webapi_.

. Create a file `webapi-svc.yml`
+
[source,yaml]
----
include::acorn-k8s:k8s:example$manifests/webapi-svc.yml[]
----

. Create the Service
+
[source,bash]
----
kubectl apply -f webapi-svc.yml
----

To check the connectivity, jump in to the cluster using the debug container and do a call.

. Start the curl container
+
[source,bash]
----
kubectl run -i --tty --rm debug --image=badouralix/curl-jq --restart=Never -- sh
----

. Call the _webapi_ API by using the Service name and port
+
[source,bash]
----
curl http://webapi-svc/webapi/items/2 | jq
----
+
Expected output:
+
[source,json]
----
{
  "item": {
    "id": 2,
    "name": "Fork",
    "serviceAddress": "items-6dd58b78ff-k7gb9/172.17.0.9:8080"
  },
  "reviews": [
    {
      "id": 3,
      "type": "item",
      "typeId": 2,
      "rating": 1,
      "ratingMin": 1,
      "ratingMax": 5,
      "comment": "This fork would not nail a ripe cheese even if its life was dependent on it",
      "serviceAddress": "reviews-76494684f5-n964t/172.17.0.10:8080"
    }
  ]
}
----

You should now see the aggregated output of the _item_ with id 2, and its single review.

Sweet success!

