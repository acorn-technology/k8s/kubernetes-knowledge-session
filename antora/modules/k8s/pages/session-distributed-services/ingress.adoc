= Exposing system to outside clients

At this point, the _webapi_, _items_ and _reviews_ applications communicate nicely, with one huge caveat: outside clients are not able to access the system!

It is of course here Ingress comes in. We would like to create an Ingress service rule supporting these requirements:

* Clients should access the application using URL http://obliquecritique.com

* When accessing the url, request should end up in _webapi_ application (clients should not be able to access _items_ and _reviews_ directly)

To achieve the first requirement, _obliquecritique.com_ must be registered to a DNS-server. We will simulate this in the same manner as in the basics hands-on session, by adding an entry to your computer's _hosts_ file.

. Take note of the k8s cluster ip-address
+
----
kubectl cluster-info

# or

minikube ip
----

. Edit `/etc/hosts` (`c:\Windows\System32\Drivers\etc\hosts` on Windows), add an entry to the hosts file with ip address from step 1:
+
----
# Example of entry in /etc/hosts
172.17.0.2 obliquecritique.com
----
+
IMPORTANT: You need to use sudo/admin-rights when editing the file.
. Save the hosts file

Next up is to create the actual Ingress service.

. Create a file `ingress.yml` with below content.
+
[source,yaml]
----
include::acorn-k8s:k8s:example$manifests/ingress.yml[]
----
+
Notable here is
+
[source,yml]
----
spec:
  rules:
    - host: obliquecritique.com <.>
      http:
        paths: <.>
          - path: /webapi
            pathType: Prefix <.>
            backend:
              service:
                name: webapi-svc <.>
                port:
                  name: webapi-svc-port <.>
----
+
<.> Binds the name `obliquecritique.com` to the rule
<.> holds a list of paths to react on and route to correct destinations. In our case, there is only one _path_ defined, for `/webapi`
<.> Each path must have a pathType. _Prefix_ will match url splitted by `/`. For example, `obliquecritique.com/webapi` will match, but not `obliquecritique.com/items`.
<.> The service name to route to
<.> The service port to use. Note that we use the name alias for the service port here (as defined in webapi-svc.yml)

. Create the Ingress
+
[source,bash]
----
kubectl apply -f ingress.yml
----

You should now be able to access the application from outside the cluster, i.e. from your local computer.

----
curl http://obliquecritique.com/webapi/items | jq
----
