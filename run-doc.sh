#!/bin/bash

# If Antora is not already installed on your local system, please install using this command:
#npm i -g @antora/cli@2.3 @antora/site-generator-default@2.3

# Also add antora-site-generator-lunr to enable searching in the documentation:
#npm i -g antora-site-generator-lunr
#npm i -g antora-lunr

# Also add asciidoctor-kroki to be able to generate plantuml:
#npm -g i asciidoctor asciidoctor-kroki

rm -rf public/*

DOCSEARCH_ENABLED=true DOCSEARCH_ENGINE=lunr NODE_PATH="$(npm -g root)" antora --fetch --cache-dir .cache/antora --generator antora-site-generator-lunr --attribute page-pagination= --to-dir public antora/antora-playbook.yml
